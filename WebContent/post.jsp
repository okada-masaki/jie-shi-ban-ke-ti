<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">

		<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
		<script type="text/javascript">
			$(function(){

				$("#category").on("keyup",function(){
					remainCount(this, 10, "#categoryCount");
				});

				$("#subject").on("keyup",function(){
					remainCount(this, 30, "#subjectCount");
				});

				$("#post").on("keyup",function(){
					remainCount(this, 1000, "#postCount");
				});
			});

			function remainCount(object, max, locate){
				var text_max = max;
				$(locate).text(text_max - $(object).val().length);

				var text_length = $(object).val().length;
				var countdown = text_max - text_length;
				$(locate).text(countdown);

				if(countdown < 0){
					$(locate).css({
						color:'#ff0000',
						fontWeight:'bold'
					});
				}else{
					$(locate).css({
						color:'#000000',
						fontWeight:'normal'
					});
				}

			}

		</script>
	</head>
	<body>
		<div class="main-contents">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			<form action="post" method="post">
				カテゴリー:<span id="categoryCount"></span>文字<br />
				<textarea name="category" id="category" cols="40" rows="1"
				placeholder="10文字以下で入力してください">${errorCategory}</textarea>
				<br />
				件名:<span id="subjectCount"></span>文字<br />
				<textarea name="subject" id="subject" cols="60" rows="1"
				placeholder="30文字以下で入力してください">${errorSubject}</textarea>
				<br />
				本文:<span id="postCount"></span>文字<br />
				<textarea name="post" id="post" cols="100" rows="10" class="tweet-box"
				placeholder="1000文字以下で入力してください">${errorPost}</textarea>
				<br />
				<input type="submit" value="投稿">（1000文字まで）
				<c:remove var="errorCategory" scope="session"/>
				<c:remove var="errorSubject" scope="session"/>
				<c:remove var="errorPost" scope="session"/>
				<a href="./">戻る</a>
			</form>
		</div>
	</body>
</html>