<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ユーザー登録情報</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript">
        <!--
        function stopCheck(){
        	if(window.confirm('停止してよろしいですか？')){
        		return true;
        	} else{
        		window.alert('キャンセルされました');
        		return false;
        	}
        }

        function activeCheck(){
        	if(window.confirm('復活してよろしいですか？')){
        		return true;
        	} else{
        		window.alert('キャンセルされました');
        		return false;
        	}
        }
        // -->
        </script>
    </head>
    <body>
    	<div class="main-contents">

    		<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			<div class="header">
				<a href="signup">新規登録</a>
			</div>

			<div class="information">

				<table border="1">
					<tr>
						<th>ＩＤ</th>
						<th>名前</th>
						<th>ログインＩＤ</th>
						<th>支店</th>
						<th>役職</th>
						<th>使用状態</th>
						<th>編集</th>
						<th>停止・復活</th>
					</tr>
					<c:forEach items="${allUsers}" var="allUser">
						<tr>
							<td><div class="id"><c:out value="${allUser.id}" /></div></td>
							<td><div class="name"><c:out value="${allUser.name}" /></div></td>
							<td><div class="loginId"><c:out value="${allUser.loginId}" /></div></td>
							<td><div class="branch"><c:out value="${allUser.branchName}" /></div></td>
							<td><div class="position"><c:out value="${allUser.positionName}" /></div></td>
							<td><c:if test="${allUser.activate == 1}">可能</c:if>
								<c:if test="${allUser.activate == 0}">不可</c:if>
							</td>
							<td><form action="management" method="post">
								<input type="hidden" name="selectId" value="${allUser.id}">
								<input type="submit" value="選択"></form>
							</td>
							<td><c:if test="${allUser.activate == 1}">
									<c:if test="${allUser.id != loginUser.id}">
										<form action="switchActivation" method="post" onSubmit="return stopCheck()">
										<input type="hidden" name="switchActivate" value="0">
										<input type="hidden" name="selectId" value="${allUser.id}">
										<input name="stop" type="submit" value="停止する"></form>
									</c:if>
								</c:if>
								<c:if test="${allUser.activate == 0}">
									<c:if test="${allUser.id != loginUser.id}">
										<form action="switchActivation" method="post" onSubmit="return activeCheck()">
										<input type="hidden" name="switchActivate" value="1">
										<input type="hidden" name="selectId" value="${allUser.id}">
										<input type="submit" value="復活する"></form>
									</c:if>
								</c:if>

							</td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<a href="./">戻る</a>
			<div class="copylight"> Copyright(c)Okada Masaki</div>
    	</div>
    </body>
</html>