<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー登録</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<form action="signup" method="post">
				<br /> <label for="name">名前</label> <input name="name" value="${user.name}" id="name" />（名前はあなたの公開プロフィールに表示されます）<br />

						<label for="loginId">ログインid</label> <input name="loginId" value="${user.loginId}" id="loginId" /> <br />
						<label for="password">パスワード</label> <input name="password" type="password" id="password" /> <br />
						<label for="checkPassword">パスワード確認</label><input name="checkPassword" type="password" id="checkPassword"><br />
						<label for="branchId">支店</label>
						<select name="branchId">
							<c:forEach items="${branches}" var="branchName">
								<option value="${branchName.id}">${branchName.name}</option>
							</c:forEach>
						</select>
						<label for="positionId">部署・役職</label>
						<select name="positionId">
							<c:forEach items="${positions}" var="positionName">
								<option value="${positionName.id}">${positionName.name}</option>
							</c:forEach>
						</select>
				<br /> <input type="submit" value="登録" /> <br /> <a href="management">戻る</a>
			</form>
			<div class="copyright">Copyright(c)Okada Masaki</div>
		</div>
	</body>
</html>