<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>掲示板</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
		<script type="text/javascript">
		<!--
        function deleteCheck(){
        	if(window.confirm('削除してよろしいですか？')){
        		return true;
        	} else{
        		window.alert('キャンセルされました');
        		return false;
        	}
        }
        // -->

		</script>
	</head>
	<body>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			<div class="header">
				<a href="./">ホーム</a>
				<a href="post">新規投稿</a>
				<a href="management">ユーザー管理</a>
				<a href="logout">ログアウト</a>
			</div>

			<div class="profile">
				<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
				<div class="loginId">
					@<c:out value="${loginUser.loginId}" />
				</div>
			</div>

			<div class="search">
				<div class="searches">
					<form action="./" method="get">
						<br>カテゴリ検索
						<textarea name="searchCategory" cols="100" rows="1">${category}</textarea>
						<br>投稿日検索<br>
						<input type="date" name="searchFromDate" min="2019/5/1" max="2019/6/31" value="${fromDate}">から
						<br>
						<input type="date" name="searchToDate" min="2019/5/1" max="2019/6/31" value="${toDate}">まで
						<br>
						<input type="submit" value="検索"><br>
					</form>
				</div>
			</div>
			<div class="posts">
				<c:forEach items="${posts}" var="post">
					<c:if test="${post.display == 1}">
				    	<div class="post">
							<div class="name">
								<span class="name">投稿者：<c:out value="${post.name}" /></span>
							</div>
							<div class="category">カテゴリー：<c:out value="${post.category}" /></div>
							<div class="subject">件名：<c:out value="${post.subject}" /></div>
							<p class="text">本文<br>${fn:replace(post.text, "
", "<br/>") }</p>
							<div class="date">投稿日時：<fmt:formatDate value="${post.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
							<form action="deletePost" method="post" onSubmit="return deleteCheck()">
								<c:if test="${loginUser.id == post.userId}">
									<input type="hidden" name="postId" value="${post.id}">
									<input type="hidden" name="display" value="0">
									<input type="submit" value="削除">
								</c:if>
							</form>
							<br />
							<div class="comment">
							<c:forEach items="${comments}" var="comment">
								<c:if test="${post.id == comment.textId}">
									<c:if test="${comment.display == 1}">
										<div class="name">
											<span class="name">　　コメント投稿者：<c:out value="${comment.name}" /></span>
										</div>
										<p class="commentText">　　コメント：<c:out value="${comment.commentText}" /></p>
										<div class="date">　　コメント日時：<fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
										<form action="deleteComment" method="post" onSubmit="return deleteCheck()">
											<c:if test="${loginUser.id == comment.userId}">
												<input type="hidden" name="commentId" value="${comment.id}">
												<input type="hidden" name="display" value="0">
												<input type="submit" value="削除">
											</c:if>
										</form>
										<br />
									</c:if>
								</c:if>
							</c:forEach>
							</div>
						</div>
						<div class="form-area">
							<c:if test="${ isShowMessageForm }">
								<form action="newComment" method="post">
									コメント<br>
									<input type="hidden" name="postId" value="${post.id}">
									<textarea name="comment" cols="100" rows="5" class="tweet-box"
									placeholder="500文字以下で入力してください"></textarea>
									<br><input type="submit" value="投稿">
								</form>
							</c:if>
						</div>
					</c:if>
				</c:forEach>
			</div>

			<div class="copylight"> Copyright(c)Okada Masaki</div>
		</div>
	</body>
</html>