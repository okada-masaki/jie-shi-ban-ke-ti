package chapter6.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Branch;
import chapter6.beans.Position;
import chapter6.beans.User;
import chapter6.service.BranchService;
import chapter6.service.PositionService;
import chapter6.service.UserService;

public class InputCheckUtil {
	public static List<String> checkInput(String inputValue, int min, int max, String itemName) {
		List<String> ret = new ArrayList<>();
		// 必須チェック
		if (StringUtils.isEmpty(inputValue)) {
			ret.add(itemName + "を入力してください");
			return ret;
		}
		if(StringUtils.isBlank(inputValue)) {
			ret.add(itemName + "は空白文字です");
			return ret;
		}
		if (inputValue.length() < min) {
			ret.add(itemName + "は" + String.valueOf(min) + "文字以上で入力してください");
			return ret;
		}
		if (inputValue.length() > max) {
			ret.add(itemName + "は" + String.valueOf(max) + "文字以内で入力してください");
			return ret;
		}
		return ret;
	}

	public static List<String> checkInputPass(String inputValue, int min, int max, String itemName) {
		List<String> ret = new ArrayList<>();
		// 必須チェック
		if (inputValue.length() < min) {
			ret.add(itemName + "は" + String.valueOf(min) + "文字以上で入力してください");
			return ret;
		}
		if (inputValue.length() > max) {
			ret.add(itemName + "は" + String.valueOf(max) + "文字以内で入力してください");
			return ret;
		}
		return ret;
	}

	public static List<String> checkName(String nameCheck) {
		return checkInput(nameCheck, 0, 10, "名前");
	}
	public static List<String> checkLoginId(String loginIdCheck) {
		return checkInput(loginIdCheck, 6, 20, "ログインID");
	}
	public static List<String> checkPassword(String passwordCheck) {
		return checkInput(passwordCheck, 6, 20, "パスワード");
	}
	public static List<String> checkPasswordSet(String passwordCheck) {
		return checkInputPass(passwordCheck, 6, 20, "パスワード");
	}
	public static List<String> checkPost(String post) {
		return checkInput(post, 0, 1000, "本文");
	}
	public static List<String> checkCategory(String category) {
		return checkInput(category, 0, 10, "カテゴリー");
	}
	public static List<String> checkSubject(String subject) {
		return checkInput(subject, 0, 30, "件名");
	}
	public static List<String> checkComment(String comment) {
		return checkInput(comment, 0, 500, "コメント");
	}

	public static List<String> checkBlank(String inputValue, String itemName) {
		List<String> ret = new ArrayList<>();

		if (StringUtils.isBlank(inputValue)) {
			ret.add(itemName + "が空白文字です");
			return ret;
		}
		return ret;
	}

	public static List<String> checkCategoryBlank(String category) {
		return checkBlank(category, "カテゴリー");
	}
	public static List<String> checkSubjectBlank(String subject) {
		return checkBlank(subject, "件名");
	}
	public static List<String> checkPostBlank(String post) {
		return checkBlank(post, "本文");
	}
	public static List<String> checkCommentBlank(String comment) {
		return checkBlank(comment, "コメント");
	}

	public static List<String> settingLoginIdDup(String loginIdCheck, int selectId){
		List<String> ret = new ArrayList<>();
		List<User> LoginIdDup = new UserService().getAllUser();
		User beforeLoginId = new UserService().getEditUser(selectId);
		List<String> LoginIds = new ArrayList<>();

		for(User user : LoginIdDup) {
			LoginIds.add(user.getLoginId());
		}

		if(!loginIdCheck.equals(beforeLoginId.getLoginId())) {
			if(LoginIds.contains(loginIdCheck)) {
				ret.add("ログインIDが重複しています");
				return ret;
			}
		}
		return ret;
	}

	public static List<String> checkLoginIdDup(String loginIdCheck){
		List<String> ret = new ArrayList<>();
		List<User> LoginIdDup = new UserService().getAllUser();
		List<String> LoginIds = new ArrayList<>();

		for(User user : LoginIdDup) {
			LoginIds.add(user.getLoginId());
		}

		if(LoginIds.contains(loginIdCheck)) {
			ret.add("ログインIDが重複しています");
			return ret;
		}
		return ret;
	}

	public static List<String> checkBranchExist(int branchCheck){
		List<String> ret = new ArrayList<>();
		List<Branch> branchExist = new BranchService().getBranch();
		List<Integer> branchIds = new ArrayList<>();

		for (Branch branch : branchExist) {
			branchIds.add(branch.getId());
		}

		if(!branchIds.contains(branchCheck)) {
			ret.add("支店は存在しません");
			return ret;
		}
		return ret;
	}

	public static List<String> checkPositionExist(int positionCheck){
		List<String> ret = new ArrayList<>();
    	List<Position> positionExist = new PositionService().getPosition();
    	List<Integer> positionIds = new ArrayList<>();

		for (Position position : positionExist) {
			positionIds.add(position.getId());
		}

		if(!positionIds.contains(positionCheck)) {
			ret.add("役職は存在しません");
			return ret;
		}
		return ret;
	}


}

