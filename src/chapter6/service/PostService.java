package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter6.beans.Post;
import chapter6.beans.UserPost;
import chapter6.dao.PostDao;
import chapter6.dao.UserPostDao;


public class PostService {

    public void register(Post post) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.insert(connection, post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<UserPost> getPost() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserPostDao postDao = new UserPostDao();
    		List<UserPost> ret = postDao.getUserMessages(connection, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public List<UserPost> getCategoryPost(String searchCategory, String searchFromDate, String searchToDate) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserPostDao postDao = new UserPostDao();
    		List<UserPost> ret = postDao.getUserCategoryMessages(connection, LIMIT_NUM, searchCategory,
    				searchFromDate, searchToDate);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public List<UserPost> getDatePost(String searchFromDate, String searchToDate) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserPostDao postDao = new UserPostDao();
    		List<UserPost> ret = postDao.getUserDateMessages(connection, LIMIT_NUM, searchFromDate, searchToDate);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public void deletePost(Post post) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		PostDao postDao = new PostDao();
    		postDao.delete(connection, post);

    		commit(connection);
    	} catch(RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


}