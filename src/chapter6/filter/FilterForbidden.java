package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

@WebFilter("/*")
public class FilterForbidden implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();
		User user = (User) session.getAttribute("loginUser");
		List<String> messages = new ArrayList<String>();

		if(!((HttpServletRequest)request).getServletPath().matches("^/css.*$")) {

	        if (user == null && !((HttpServletRequest)request).getServletPath().equals("/login")) {
	        	messages.add("ログインしてください");
	        	session.setAttribute("errorMessages", messages);
	            ((HttpServletResponse)response).sendRedirect("login");
	    		return;

	        }

	        if(((HttpServletRequest)request).getServletPath().equals("/management") ||
	        		((HttpServletRequest)request).getServletPath().equals("/signup") ||
	        		((HttpServletRequest)request).getServletPath().equals("/settings")) {

	        	if(user.getBranchId() != 1 || user.getPositionId() != 1) {
	        		messages.add("アクセス権限がありません");
	        		session.setAttribute("errorMessages", messages);
	        		((HttpServletResponse)response).sendRedirect("./");
	        		return;
	        	}
	        }
		}
		chain.doFilter(request, response); // サーブレットを実行

	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ

	}

}