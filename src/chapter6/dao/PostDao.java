package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter6.beans.Post;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class PostDao {

    public void insert(Connection connection, Post post) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO posts ( ");
            sql.append("user_id");
            sql.append(", category");
            sql.append(", subject");
            sql.append(", text");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); // category
            sql.append(", ?"); // subject
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, post.getUserId());
            ps.setString(2, post.getCategory());
            ps.setString(3, post.getSubject());
            ps.setString(4, post.getText());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void delete(Connection connection, Post post) {

    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("UPDATE posts SET ");
    		sql.append("display = 0");
    		sql.append(", updated_date = CURRENT_TIMESTAMP");
    		sql.append(" WHERE");
    		sql.append(" id = ?");

    		ps = connection.prepareStatement(sql.toString());

    		ps.setInt(1, post.getId());

    		int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
    	} catch(SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
		close(ps);
    	}
    }

}