package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Position;
import chapter6.exception.SQLRuntimeException;

public class PositionDao{

	public List<Position> getPositionName(Connection connection){
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT positions.position_name as name, ");
			sql.append("positions.id as id ");
			sql.append("FROM positions");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Position> ret = toPositionList(rs);
			return ret;
		} catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<Position> toPositionList(ResultSet rs)
	        throws SQLException {

		List<Position> ret = new ArrayList<Position>();
		try {
			while(rs.next()) {
				String position = rs.getString("name");
				int id = rs.getInt("id");

				Position positionName = new Position();

				positionName.setName(position);
				positionName.setId(id);

				ret.add(positionName);
			}
			return ret;
		} finally {
            close(rs);
        }
	}
}