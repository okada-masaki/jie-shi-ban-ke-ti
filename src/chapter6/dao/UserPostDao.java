package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserPost;
import chapter6.exception.SQLRuntimeException;

public class UserPostDao {

    public List<UserPost> getUserMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.user_id as user_id, ");
            sql.append("posts.category as category, ");
            sql.append("posts.subject as subject, ");
            sql.append("posts.text as text, ");
            sql.append("posts.display as display, ");
            sql.append("users.name as name, ");
            sql.append("posts.created_date as created_date ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    public List<UserPost> getUserCategoryMessages(Connection connection, int num,
    		String searchCategory, String searchFromDate, String searchToDate) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.user_id as user_id, ");
            sql.append("posts.category as category, ");
            sql.append("posts.subject as subject, ");
            sql.append("posts.text as text, ");
            sql.append("posts.display as display, ");
            sql.append("users.name as name, ");
            sql.append("posts.created_date as created_date ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("WHERE (category ");
            sql.append("LIKE ?) ");
            sql.append("AND (posts.created_date BETWEEN ? AND ?) ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, "%" + searchCategory + "%");
            ps.setString(2, searchFromDate + " 00:00:00");
            ps.setString(3, searchToDate + " 23:59:59");

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<UserPost> getUserDateMessages(Connection connection, int num,
    		String searchFromDate, String searchToDate) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.user_id as user_id, ");
            sql.append("posts.category as category, ");
            sql.append("posts.subject as subject, ");
            sql.append("posts.text as text, ");
            sql.append("posts.display as display, ");
            sql.append("users.name as name, ");
            sql.append("posts.created_date as created_date ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("WHERE posts.created_date ");
            sql.append("BETWEEN ? AND ? ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, searchFromDate + " 00:00:00");
            ps.setString(2, searchToDate + " 23:59:59");

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserPost> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserPost> ret = new ArrayList<UserPost>();
        try {
            while (rs.next()) {

            	int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                int display = rs.getInt("display");
                String name = rs.getString("name");
                String category = rs.getString("category");
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserPost post = new UserPost();

                post.setId(id);
                post.setUserId(userId);
                post.setDisplay(display);
                post.setName(name);
                post.setCategory(category);
                post.setSubject(subject);
                post.setText(text);
                post.setCreated_date(createdDate);

                ret.add(post);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}