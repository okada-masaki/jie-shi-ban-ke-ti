package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Branch;
import chapter6.exception.SQLRuntimeException;

public class BranchDao{

	public List<Branch> getBranchName(Connection connection){
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT branches.branch_name as name, ");
			sql.append("branches.id as id ");
			sql.append("FROM branches");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Branch> ret = toBranchList(rs);
			return ret;
		} catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<Branch> toBranchList(ResultSet rs)
	        throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while(rs.next()) {
				String branch = rs.getString("name");
				int id = rs.getInt("id");

				Branch branchName = new Branch();

				branchName.setName(branch);
				branchName.setId(id);

				ret.add(branchName);
			}
			return ret;
		} finally {
            close(rs);
        }
	}
}