package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserComment;
import chapter6.exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> getUserMessages(Connection connection, int num){

		PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.comment_text as text, ");
            sql.append("comments.text_id as text_id, ");
            sql.append("comments.display as display, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("INNER JOIN posts ");
            sql.append("ON comments.text_id = posts.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
        	throw new SQLRuntimeException(e);
        } finally {
        	close(ps);
        }
	}

	private List<UserComment> toUserMessageList(ResultSet rs)
			throws SQLException{

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while(rs.next()) {

				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				int textId = rs.getInt("text_id");
				int display = rs.getInt("display");
				String name = rs.getString("name");
				String commentText = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserComment comment = new UserComment();

				comment.setId(id);
				comment.setUserId(userId);
				comment.setTextId(textId);
				comment.setDisplay(display);
				comment.setName(name);
				comment.setCommentText(commentText);
				comment.setCreated_date(createdDate);

				ret.add(comment);

			}
			return ret;
		} finally {
			close(rs);
		}
	}
}