package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter6.beans.Comment;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
            sql.append("user_id");
            sql.append(", text_id");
            sql.append(", comment_text");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); // text_id
            sql.append(", ?"); // comment
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getUserId());
            ps.setInt(2, comment.getTextId());
            ps.setString(3, comment.getCommentText());

            ps.executeUpdate();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		}finally {
			close(ps);
		}
	}

	public void delete(Connection connection, Comment comment) {

    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("UPDATE comments SET ");
    		sql.append("display = 0");
    		sql.append(", updated_date = CURRENT_TIMESTAMP");
    		sql.append(" WHERE");
    		sql.append(" id = ?");

    		ps = connection.prepareStatement(sql.toString());

    		ps.setInt(1, comment.getId());

    		int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
    	} catch(SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
		close(ps);
    	}
    }
}