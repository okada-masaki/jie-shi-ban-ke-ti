package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getPositionId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User getUser(Connection connection, String loginId,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, loginId);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                String password = rs.getString("password");
                int branchId = rs.getInt("branch_id");
                int positionId = rs.getInt("position_id");
                int activate = rs.getInt("activate");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");


                User user = new User();
                user.setId(id);
                user.setLoginId(loginId);
                user.setName(name);
                user.setPassword(password);
                user.setBranchId(branchId);
                user.setPositionId(positionId);
                user.setActivate(activate);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public User getUser(Connection connection, int id) {

    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM users WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, id);

    		ResultSet rs = ps.executeQuery();
    		List<User> userList = toUserList(rs);
    		if (userList.isEmpty() == true) {
    			return null;
    		} else if (2 <= userList.size()) {
    			throw new IllegalStateException("2 <= userList.size()");
    		} else {
    			return userList.get(0);
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

    public User getEditUser(Connection connection, int selectedId) {

    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("SELECT ");
    		sql.append("users.id as id, ");
			sql.append("users.name as name, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.password as password, ");
			sql.append("users.branch_id as branch_id, ");
			sql.append("users.position_id as position_id, ");
			sql.append("branches.branch_name as branch_name, ");
			sql.append("positions.position_name as position_name ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");
			sql.append("WHERE ");
			sql.append("users.id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, selectedId);

			ResultSet rs = ps.executeQuery();
			List<User> editUserList = toEditUserList(rs);
			if (editUserList.isEmpty() == true) {
                return null;
            } else if (2 <= editUserList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return editUserList.get(0);
            }
    	} catch(SQLException e) {
    		throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toEditUserList(ResultSet rs)
    		throws SQLException {

    	List<User> ret = new ArrayList<User>();
    	try {
    		while(rs.next()) {

    			int id = rs.getInt("id");
    			String name = rs.getString("name");
    			String loginId = rs.getString("login_id");
    			String password = rs.getString("password");
    			int branchId = rs.getInt("branch_id");
    			int positionId = rs.getInt("position_id");
    			String branchName = rs.getString("branch_name");
    			String positionName = rs.getString("position_name");

    			User editUser = new User();

    			editUser.setId(id);
    			editUser.setName(name);
    			editUser.setLoginId(loginId);
    			editUser.setPassword(password);
    			editUser.setBranchId(branchId);
    			editUser.setPositionId(positionId);
    			editUser.setBranchName(branchName);
    			editUser.setPositionName(positionName);

    			ret.add(editUser);
    		}
    		return ret;
    	} finally {
    		close(rs);
    	}
    }

    public List<User> getAllUser(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.name as name, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.activate as activate, ");
			sql.append("branches.branch_name as branchName, ");
			sql.append("positions.position_name as positionName ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");
			sql.append("ORDER BY id ASC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toAllUserList(rs);
			return ret;
		} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

    private List<User> toAllUserList(ResultSet rs)
            throws SQLException {

    	List<User> ret = new ArrayList<User>();
    	try {
    		while(rs.next()) {

    			int id = rs.getInt("id");
                String name = rs.getString("name");
                String loginId = rs.getString("login_id");
                int activate = rs.getInt("activate");
                String branchName = rs.getString("branchName");
                String positionName = rs.getString("positionName");

                User allUser = new User();

                allUser.setId(id);
                allUser.setName(name);
                allUser.setLoginId(loginId);
                allUser.setActivate(activate);
                allUser.setBranchName(branchName);
                allUser.setPositionName(positionName);

                ret.add(allUser);
    		}
    		return ret;
    	} finally {
    		close(rs);
    	}
    }

    public void switchActivate(Connection connection, User userActivate) {

    	PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" activate = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, userActivate.getActivate());
			ps.setInt(2, userActivate.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
    }

    public void update(Connection connection, User editUser) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" name = ?");
			sql.append(", login_id = ?");
			if(StringUtils.isNotEmpty(editUser.getPassword()) && StringUtils.isNotEmpty(editUser.getCheckPassword())) {
				sql.append(", password = ?");
			}
			sql.append(", branch_id = ?");
			sql.append(", position_id = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			int i = 1;

			ps.setString(i++, editUser.getName());
			ps.setString(i++, editUser.getLoginId());
			if(StringUtils.isNotEmpty(editUser.getPassword()) && StringUtils.isNotEmpty(editUser.getCheckPassword())) {
				ps.setString(i++, editUser.getPassword());
			}
			ps.setInt(i++, editUser.getBranchId());
			ps.setInt(i++, editUser.getPositionId());
			ps.setInt(i++, editUser.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
}