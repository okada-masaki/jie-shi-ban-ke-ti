package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Branch;
import chapter6.beans.Position;
import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.BranchService;
import chapter6.service.PositionService;
import chapter6.service.UserService;
import chapter6.utils.InputCheckUtil;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        int selectId = (int) session.getAttribute("selectId");

        User editUser = new UserService().getEditUser(selectId);
        List<Branch> branchName = new BranchService().getBranch();
    	List<Position> positionName = new PositionService().getPosition();

    	session.setAttribute("branches", branchName);
    	session.setAttribute("positions", positionName);
        session.setAttribute("editUser", editUser);

        request.getRequestDispatcher("settings.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);
		User user = (User) request.getSession().getAttribute("loginUser");

		int editUserId = editUser.getId();
		int loginUserId = user.getId();

		if (isValid(request, messages) == true) {
			if(editUser.getPassword().equals(editUser.getCheckPassword())) {
				try {
					new UserService().update(editUser);

					if(editUserId == loginUserId) {
						session.setAttribute("loginUser", editUser);
					}

					response.sendRedirect("management");
				} catch (NoRowsUpdatedRuntimeException e) {
					messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
					session.setAttribute("errorMessages", messages);
					session.setAttribute("editUser", editUser);
					response.sendRedirect("settings");
					return;
				}
			} else {
				messages.add("入力したパスワードと確認用パスワードが一致しません");
            	session.setAttribute("errorMessages", messages);
            	session.setAttribute("editUser", editUser);
            	response.sendRedirect("settings");
			}
		} else {
			session.setAttribute("errorMessages", messages);
			session.setAttribute("editUser", editUser);
			response.sendRedirect("settings");
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		User user = (User) request.getSession().getAttribute("loginUser");

		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setCheckPassword(request.getParameter("checkPassword"));
		if(editUser.getId() == user.getId()) {
			editUser.setBranchId(user.getBranchId());
			editUser.setPositionId(user.getPositionId());
		} else {
			editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			editUser.setPositionId(Integer.parseInt(request.getParameter("positionId")));
		}

		return editUser;
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginIdCheck = request.getParameter("loginId");
		String passwordCheck = request.getParameter("password");
		String nameCheck = request.getParameter("name");
		int branchCheck = Integer.parseInt(request.getParameter("branchId"));
		int positionCheck = Integer.parseInt(request.getParameter("positionId"));
		int selectId = Integer.parseInt(request.getParameter("id"));

		messages.addAll(InputCheckUtil.checkName(nameCheck));
        messages.addAll(InputCheckUtil.checkLoginId(loginIdCheck));
        messages.addAll(InputCheckUtil.settingLoginIdDup(loginIdCheck, selectId));

        if(StringUtils.isNotEmpty(passwordCheck) && StringUtils.isNotEmpty(passwordCheck)) {
            messages.addAll(InputCheckUtil.checkPasswordSet(passwordCheck));
        }

        messages.addAll(InputCheckUtil.checkBranchExist(branchCheck));
        messages.addAll(InputCheckUtil.checkPositionExist(positionCheck));

        if(!(loginIdCheck.matches("^[0-9a-zA-Z]+$"))) {
        	messages.add("ログインIDは半角英数字で入力してください");
        }
        if(StringUtils.isNotEmpty(passwordCheck)) {
	        if(!(passwordCheck.matches("^[0-9a-zA-Z -/:-@\\[-\\`\\{-\\~]+$"))){
	        	messages.add("パスワードは記号を含む半角文字で入力してください");
	        }
		}
        if(branchCheck == 1){
        	if(positionCheck == 3 || positionCheck == 4) {
        		messages.add("支店と部署の組み合わせが不正です");
        	}
        }
        if(branchCheck >= 2 && branchCheck <=4){
        	if(positionCheck == 1 || positionCheck == 2) {
        		messages.add("支店と部署の組み合わせが不正です");
        	}
        }

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}