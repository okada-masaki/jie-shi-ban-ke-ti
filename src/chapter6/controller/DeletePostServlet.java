package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.Post;
import chapter6.beans.User;
import chapter6.service.PostService;

@WebServlet(urlPatterns = { "/deletePost" })
public class DeletePostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("delete.jsp").forward(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("loginUser");

		Post deletePost = new Post();

		deletePost.setUserId(user.getId());
		deletePost.setDisplay(Integer.parseInt(request.getParameter("display")));
		deletePost.setId(Integer.parseInt(request.getParameter("postId")));

		new PostService().deletePost(deletePost);

		response.sendRedirect("./");
	}
}