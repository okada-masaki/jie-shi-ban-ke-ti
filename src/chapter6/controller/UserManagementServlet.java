package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class UserManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<User> allUser = new UserService().getAllUser();

    	request.setAttribute("allUsers", allUser);

    	request.getRequestDispatcher("/management.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
    		HttpServletResponse response) throws IOException, ServletException {

    	int selectId = Integer.parseInt(request.getParameter("selectId"));

    	HttpSession session = request.getSession();
    	session.setAttribute("selectId", selectId);

    	response.sendRedirect("settings");
    }
}