package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserPost;
import chapter6.service.CommentService;
import chapter6.service.PostService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        String searchCategory = request.getParameter("searchCategory");
        String searchFromDate = request.getParameter("searchFromDate");
        String searchToDate = request.getParameter("searchToDate");

        if(StringUtils.isEmpty(searchFromDate)) {
        	searchFromDate = "1900/1/1";
        }
        if(StringUtils.isEmpty(searchToDate)) {
        	searchToDate = "2050/1/1";
        }

        if(StringUtils.isNotEmpty(searchCategory)) {

        	List<UserPost> post = new PostService().getCategoryPost(searchCategory, searchFromDate, searchToDate);
        	List<UserComment> comment = new CommentService().getComment();

        	request.setAttribute("category", searchCategory);
        	request.setAttribute("posts", post);
        	request.setAttribute("comments", comment);
        	request.setAttribute("isShowMessageForm", isShowMessageForm);

        	request.getRequestDispatcher("/top.jsp").forward(request, response);
        } else if(!StringUtils.isEmpty(searchFromDate) && !StringUtils.isEmpty(searchToDate)) {

        	List<UserPost> post = new PostService().getDatePost(searchFromDate, searchToDate);
        	List<UserComment> comment = new CommentService().getComment();

        	request.setAttribute("fromDate", searchFromDate);
        	request.setAttribute("toDate", searchToDate);
        	request.setAttribute("posts", post);
        	request.setAttribute("comments", comment);
        	request.setAttribute("isShowMessageForm", isShowMessageForm);

        	request.getRequestDispatcher("/top.jsp").forward(request, response);
        } else {
	        List<UserPost> post = new PostService().getPost();
	        List<UserComment> comment = new CommentService().getComment();

	        request.setAttribute("posts", post);
	        request.setAttribute("comments", comment);
	        request.setAttribute("isShowMessageForm", isShowMessageForm);

	        request.getRequestDispatcher("/top.jsp").forward(request, response);
        }
    }
}