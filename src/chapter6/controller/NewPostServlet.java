package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.Post;
import chapter6.beans.User;
import chapter6.service.PostService;
import chapter6.utils.InputCheckUtil;

@WebServlet(urlPatterns = { "/post" })
public class NewPostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
    		HttpServletResponse response) throws IOException, ServletException {

    	request.getRequestDispatcher("post.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        User user = (User) session.getAttribute("loginUser");

        Post post = new Post();

        post.setUserId(user.getId());
        post.setCategory(request.getParameter("category"));
        post.setSubject(request.getParameter("subject"));
        post.setText(request.getParameter("post"));

        if (isValid(request, messages) == true) {

            new PostService().register(post);

            response.sendRedirect("./");
        } else {

            String errorCategory = request.getParameter("category");
            String errorSubject = request.getParameter("subject");
            String errorPost = request.getParameter("post");

            session.setAttribute("errorCategory", errorCategory);
            session.setAttribute("errorSubject", errorSubject);
            session.setAttribute("errorPost", errorPost);
            session.setAttribute("errorMessages", messages);

            response.sendRedirect("post");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String post = request.getParameter("post");
        String category = request.getParameter("category");
        String subject = request.getParameter("subject");

        messages.addAll(InputCheckUtil.checkCategory(category));
        messages.addAll(InputCheckUtil.checkSubject(subject));
        messages.addAll(InputCheckUtil.checkPost(post));

        messages.addAll(InputCheckUtil.checkCategoryBlank(category));
        messages.addAll(InputCheckUtil.checkSubjectBlank(subject));
        messages.addAll(InputCheckUtil.checkPostBlank(post));

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}