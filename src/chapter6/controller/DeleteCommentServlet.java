package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentService;

@WebServlet(urlPatterns = { "/deleteComment" })
public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("delete.jsp").forward(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("loginUser");

		Comment deleteComment = new Comment();

		deleteComment.setUserId(user.getId());
		deleteComment.setDisplay(Integer.parseInt(request.getParameter("display")));
		deleteComment.setId(Integer.parseInt(request.getParameter("commentId")));

		new CommentService().deleteComment(deleteComment);

		response.sendRedirect("./");
	}
}