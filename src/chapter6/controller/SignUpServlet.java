package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.Branch;
import chapter6.beans.Position;
import chapter6.beans.User;
import chapter6.service.BranchService;
import chapter6.service.PositionService;
import chapter6.service.UserService;
import chapter6.utils.InputCheckUtil;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<Branch> branchName = new BranchService().getBranch();
    	List<Position> positionName = new PositionService().getPosition();

    	request.setAttribute("branches", branchName);
    	request.setAttribute("positions", positionName);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLoginId(request.getParameter("loginId"));
            user.setName(request.getParameter("name"));
            user.setPassword(request.getParameter("password"));
            user.setCheckPassword(request.getParameter("checkPassword"));
            user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
            user.setPositionId(Integer.parseInt(request.getParameter("positionId")));

            session.setAttribute("users", user);

            if(!(user.getPassword().equals(user.getCheckPassword()))) {
            	messages.add("パスワードが正しくありません。");
            	session.setAttribute("errorMessages", messages);
            	response.sendRedirect("signup");
            } else {
            	new UserService().register(user);

            	response.sendRedirect("management");
            }
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String loginIdCheck = request.getParameter("loginId");
        String passwordCheck = request.getParameter("password");
        String nameCheck = request.getParameter("name");
        int branchCheck = Integer.parseInt(request.getParameter("branchId"));
		int positionCheck = Integer.parseInt(request.getParameter("positionId"));


        messages.addAll(InputCheckUtil.checkName(nameCheck));
        messages.addAll(InputCheckUtil.checkLoginId(loginIdCheck));
        messages.addAll(InputCheckUtil.checkLoginIdDup(loginIdCheck));
        messages.addAll(InputCheckUtil.checkPassword(passwordCheck));

        messages.addAll(InputCheckUtil.checkBranchExist(branchCheck));
        messages.addAll(InputCheckUtil.checkPositionExist(positionCheck));

        if(!(loginIdCheck.matches("^[0-9a-zA-Z]+$"))) {
        	messages.add("ログインIDは半角英数字で入力してください");
        }
        if(!(passwordCheck.matches("^[0-9a-zA-Z -/:-@\\[-\\`\\{-\\~]+$"))){
        	messages.add("パスワードは記号を含む半角文字で入力してください");
        }
        if(branchCheck == 1){
        	if(positionCheck == 3 || positionCheck == 4) {
        		messages.add("支店と部署の組み合わせが不正です");
        	}
        }
        if(branchCheck >= 2 && branchCheck <=4){
        	if(positionCheck == 1 || positionCheck == 2) {
        		messages.add("支店と部署の組み合わせが不正です");
        	}
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}