package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentService;
import chapter6.utils.InputCheckUtil;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		request.getRequestDispatcher("comment.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if(isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Comment comment = new Comment();

			comment.setUserId(user.getId());
			comment.setTextId(Integer.parseInt(request.getParameter("postId")));
			comment.setCommentText(request.getParameter("comment"));

			new CommentService().register(comment);

			response.sendRedirect("./");
		}else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String comment = request.getParameter("comment");

		messages.addAll(InputCheckUtil.checkComment(comment));
		messages.addAll(InputCheckUtil.checkCommentBlank(comment));

		if(messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}
}